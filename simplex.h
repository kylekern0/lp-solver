/* Copyright (c) 2019 Kyle Kern <kylekern0@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. */

/* simplex.h:
 * * Simplex Algorithm function declarations */

#ifndef __SIMPLEX_H__
#define __SIMPLEX_H__

/* Project includes: */
#include "bigm.h"

/* Checks if the variable in a given column is a basic variable: */
int check_basic (bigm **tableau, unsigned rowc, unsigned varn);

/* Determines the column number for the next simplex algo pivot column: */
unsigned pivot_col (bigm **tableau, unsigned varc, unsigned conc, int sense);

/* Determines the row number for the next simplex algo pivot row: */
unsigned pivot_row (bigm **tableau, unsigned varc, unsigned conc, unsigned artc, unsigned pcol);

/* Solves a linear programming problem via the simplex algorithm: */
void simplex (bigm **tableau, unsigned varc, unsigned conc, unsigned artc, int sense);

/* Extracts variable values from final tableau: */
void parse_tableau (bigm **tableau, unsigned varc, unsigned conc, unsigned artc, double *values);

#endif // __SIMPLEX_H__
