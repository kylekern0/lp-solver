/* Copyright (c) 2019 Kyle Kern <kylekern0@protonmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. */

/* bigm.c:
 * * Big M expression arithmetic and comparison implementation */

/* Project includes: */
#include "bigm.h"

/* Big M notation constants: */
const bigm _0m0 = {0, 0};              /* 0m + 0 */
const bigm _0m1 = {0, 1};              /* 0m + 1 */
const bigm _1m0 = {1, 0};              /* 1m + 0 */
const bigm MAXM = {DBL_MAX, DBL_MAX};  /* Largest possible value */

/* multm:
 * * Multiplies a Big M expression a by a constant b
 * 
 * Inputs:
 * * a (bigm):   Big M expression
 * * b (double): Constant factor to the Big M expression
 * 
 * Outputs: None
 * 
 * Return Value: (bigm) product of a and b
 */
bigm multm (bigm a, double b)
{
	bigm c;
	c.m = a.m * b;
	c.c = a.c * b;
	return c;
}

/* addm:
 * * Add two Big M expressions 
 * 
 * Inputs:
 * * a (bigm): First Big M expression to add 
 * * b (bigm): Second Big M expression to add
 * 
 * Outputs: None
 * 
 * Return Value: (bigm) Sum of a and b
 */
bigm addm (bigm a, bigm b)
{
	bigm c;
	c.m = a.m + b.m;
	c.c = a.c + b.c;
	return c;
}

/* ltm:
 * * Less-than comparison for Big M expressions
 * 
 * Inputs:
 * * a (bigm): First Big M expression to compare
 * * b (bigm): Second Big M expression to compare
 * 
 * Outputs: None
 * 
 * Return Value: (int) 1 if a < b, 0 otherwise
 */
int ltm (bigm a, bigm b)
{
	/* Since M is arbitrarily large, a is automatically less than b if its M coefficient is less than b's */
	if (a.m < b.m)
	{
		return 1;
	}
	
	/* If instead a's M coefficient is larger than b's, a is automatically greater */
	else if (a.m > b.m)
	{
		return 0;
	}
	
	/* Otherwise, if both M coefficients are equal, then the constant terms determine the relationship: */
	else
	{
		if (a.c < b.c)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* gtm:
 * * Greater-than comparison for Big M expressions
 * 
 * Inputs:
 * * a (bigm): First Big M expression to compare
 * * b (bigm): Second Big M expression to compare
 * 
 * Outputs: None
 * 
 * Return Value: (int) 1 if a > b, 0 otherwise
 */
int gtm (bigm a, bigm b)
{
	/* Since M is arbitrarily large, a is automatically greater than b if its M coefficient is greater than b's */
	if (a.m > b.m)
	{
		return 1;
	}
	
	/* If instead a's M coefficient is smaller than b's, a is automatically less */
	else if (a.m < b.m)
	{
		return 0;
	}
	
	/* Otherwise, if both M coefficients are equal, then the constant terms determine the relationship: */
	else
	{
		if (a.c > b.c)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
}

/* eqm:
 * * Equal-to comparison for Big M expressions
 * 
 * Inputs:
 * * a (bigm): First Big M expression to compare
 * * b (bigm): Second Big M expression to compare
 * 
 * Outputs: None
 * 
 * Return Value: (int) 1 if a == b, 0 otherwise
 */
int eqm (bigm a, bigm b)
{
	/* a == b iff both its M-coefficient and constant term are equal */
	return ((a.m == b.m) && (a.c == b.c)) ? 1 : 0;
}

/* ltem:
 * * Less-than-or-equal comparison for Big M expressions
 * 
 * Inputs:
 * * a (bigm): First Big M expression to compare
 * * b (bigm): Second Big M expression to compare
 * 
 * Outputs: None
 * 
 * Return Value: (int) 1 if a <= b, 0 otherwise
 */
int ltem (bigm a, bigm b)
{
	return (ltm (a, b) || eqm (a, b));
}

/* gtem:
 * * Greater-than-or-equal comparison for Big M expressions
 * 
 * Inputs:
 * * a (bigm): First Big M expression to compare
 * * b (bigm): Second Big M expression to compare
 * 
 * Outputs: None
 * 
 * Return Value: (int) 1 if a >= b, 0 otherwise
 */
int gtem (bigm a, bigm b)
{
	return (gtm (a, b) || eqm (a, b));
}
