/* Copyright (c) 2019 Kyle Kern <kylekern0@protonmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. */

/* globals.h:
 * * Global flag declarations */

#ifndef __GLOBALS_H__
#define __GLOBALS_H__

extern int PRINT_MTERMS;        /* 1 to enable printing the M-coefficients in the table, 0 otherwise */
extern int PRINT_ARTIFICIAL;    /* 1 to enable printing the tableau with artificial variables, 0 otherwise */
extern int PRINT_FINAL;         /* 1 to enable printing the final tableau, 0 otherwise */
extern int PRINT_INTERMEDIATE;  /* 1 to enable printing all non-initial tableaus, 0 otherwise */
extern int PRINT_OPERATIONS;    /* 1 to enable printing row operations between tableaus, 0 otherwise */

#endif // __GLOBALS_H__
