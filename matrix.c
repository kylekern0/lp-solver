/* Copyright (c) 2019 Kyle Kern <kylekern0@protonmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. */

/* matrix.c:
 * * Elementary row operations function implementations */

#include "matrix.h"

/* ero_interchange:
 * * Interchange two rows in the matrix
 * 
 * Inputs:
 * * matrix (bigm**):   2D matrix upon which to operate
 * *         1st index: Row number
 * *         2nd index: Column number
 * * rowsz  (unsigned): Number of columns in each row
 * * row1   (unsigned): One of two row numbers to swap positions
 * * row2   (unsigned): Second of two row numbers to swap positions
 * 
 * Outputs:
 * * matrix (bigm**): Modified matrix
 * 
 * Return Value: None
 */
void ero_interchange (bigm **matrix, unsigned rowsz, unsigned row1, unsigned row2)
{
	/* Internal storage: */
	unsigned col;  /* Column number under examination */
	bigm     tmp;  /* Value being moved */

	/* Iterate over each column to swap the values between rows: */
	for (col = 0; col < rowsz; col++)
	{
		tmp = matrix[row1][col];
		matrix[row1][col] = matrix[row2][col];
		matrix[row2][col] = tmp;
	}
}

/* ero_product:
 * * Multiply each row element by a nonzero constant:
 * 
 * Inputs:
 * * matrix (bigm**):   2D matrix upon which to operate
 * *         1st index: Row number
 * *         2nd index: Column number
 * * rowsz  (unsigned): Number of columns in each row
 * * dst    (unsigned): Row number to operate upon
 * * factor (double):   Value multiplied to elements of destination row
 * 
 * Outputs:
 * * matrix (bigm**): Modified matrix
 * 
 * Return Value: None
 */
void ero_product (bigm **matrix, unsigned rowsz, unsigned dst, double factor)
{
	/* Internal storage: */
	unsigned col;  /* Column number under examination */

	/* Iterate over each column and multiply the cell by the factor: */
	for (col = 0; col < rowsz; col++)
	{
		matrix[dst][col] = multm (matrix[dst][col], factor);
	}
}

/* ero_sumproduct:
 * * Multiply a row by a constant, then add to another row
 * 
 * Inputs:
 * * matrix (double**): 2D matrix upon which to operate
 * *         1st index: Row number
 * *         2nd index: Column number
 * * rowsz  (unsigned): Number of columns in each row
 * * dst    (unsigned): Destination row number
 * * src    (unsigned): Source row number
 * * factor (double):   Value multiplied to elements of source row
 * 
 * Outputs:
 * * matrix (double**): Modified matrix
 * 
 * Return Value: none
 */
void ero_sumproduct (bigm **matrix, unsigned rowsz, unsigned dst, unsigned src, double factor)
{
	/* Internal storage: */
	unsigned col;  /* Column number under examination */
	bigm     tmp;  /* Value being added */

	/* Iterate over each column and add the product from the source to the destination: */
	for (col = 0; col < rowsz; col++)
	{
		tmp = multm (matrix[src][col], factor);
		matrix[dst][col] = addm (matrix[dst][col], tmp);
	}
}

/* ero_sumproduct_m:
 * * Sum-product logic for Big M factors
 * 
 * Inputs:
 * * matrix (double**): 2D matrix upon which to operate
 * *         1st index: Row number
 * *         2nd index: Column number
 * * rowsz  (unsigned): Number of columns in each row
 * * dst    (unsigned): Destination row number
 * * src    (unsigned): Source row number
 * * factor (bigm):     Value multiplied to elements of source row
 * 
 * Outputs:
 * * matrix (double**): Modified matrix
 * 
 * Return Value: none
 */
void ero_sumproduct_m (bigm **matrix, unsigned rowsz, unsigned dst, unsigned src, bigm factor)
{
	/* Internal storage: */
	unsigned col;  /* Column number under examination */
	bigm     tmp;  /* Value being added */

	/* Iterate over each column and add the product from the source to the destination: */
	for (col = 0; col < rowsz; col++)
	{
		tmp = multm (factor, matrix[src][col].c);
		matrix[dst][col] = addm (matrix[dst][col], tmp);
	}
}
