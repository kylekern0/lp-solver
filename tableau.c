/* Copyright (c) 2019 Kyle Kern <kylekern0@protonmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. */

/* tableau.c:
 * * Simplex tableau generation and validation function implementations */

/* Project includes: */
#include "tableau.h"

/* Standard Library includes: */
#include <stdlib.h>
#include <string.h>

/* count_constraints:
 * * Counts the number of constraint rows in the file
 * 
 * Inputs:
 * * csv (FILE*): Pointer to a pre-opened file stream for the CSV file
 * 
 * Outputs: None
 * 
 * Return Value: (unsigned) 0 for error, otherwise number of constraint rows
 */
unsigned count_constraints (FILE *csv)
{
	/* Internal storage: */
	unsigned conc;  /* Number of constraints */
	char     chin;  /* Character read from file */
	
	/* Rewind the file pointer to ensure we start reading from the beginning: */
	rewind (csv);
	
	/* Each constraint requires its own line below the net evaluation row - count newlines to count constraints: */
	for (conc = 0; chin = fgetc (csv), chin != EOF;)
	{
		if (chin == NEWLN)
		{
			conc++;
		}
	}
	
	/* Correct for counting the net evaluation row: */
	conc--;
	
	/* Return the number of constraint rows encountered: */
	return conc;
}

/* count_variables:
 * * Counts the number of decision variables in the file
 * 
 * Inputs:
 * * csv  (FILE*):    Pointer toa pre-opened file stream for the CSV file
 * * conc (unsigned): Number of constraints
 * 
 * Outputs: None
 * 
 * Return Value: (unsigned) 0 for error, otherwise number of decision variables
 */
unsigned count_variables (FILE *csv, unsigned conc)
{
	/* Internal storage: */
	unsigned varc;  /* Number of decision variables */
	char     chin;  /* Character read from file */
	
	/* Rewind the file pointer to ensure we start reading from the beginning: */
	rewind (csv);
	
	/* Each variable requires its own comma-delimited field in the net evaluation row - count commas to count variables: */
	for (varc = 1; chin = fgetc (csv), (chin != NEWLN) && (chin != EOF); varc += ((chin == ',') ? 1 : 0));
	
	/* Subtract the number of objective variables, slack variables (constraints), and right-hand-sides: */
	varc -= (1 + conc + 1);
	
	/* Return the number of decision variables encountered: */
	return varc;
}

/* csv_to_tab:
 * * Imports a simplex tableau from comma-deliminated file
 * 
 * Inputs:
 * * tableau (bigm**):   Pointer to a pre-allocated 2D array for tableau storage
 * *          1st index: Row number
 * *          2nd index: Column number
 * * csv     (FILE*):    Pointer to a pre-opened file stream for the CSV file
 * * varc    (unsigned): Number of decision variables
 * * conc    (unsigned): Number of constraints
 * 
 * Outputs:
 * * tableau (double**): Simplex tableau in numerical form
 * 
 * Return Value: None
 */
void csv_to_tab (bigm **tableau, FILE *csv, unsigned varc, unsigned conc)
{
	/* Internal storage: */
	unsigned rowsz;                      /* Number of cells in one row */
	unsigned row;                        /* Row iterator */
	unsigned col;                        /* Column iterator */
	char     cell[MAX_CELL_CHARS] = "";  /* Text read from one cell of the CSV file */
	char     rc;                         /* Latest character read from file */
	char     rstr[2] = "0";              /* Latest character read from file, in string form */
	double   value;                      /* Numerical representation of the cell's contents */
	
	
	/* Calculate the size of each row based on the numbers of variables and constraints: */
	rowsz = 1 + varc + conc + 1;
	
	/* Rewind the file pointer to ensure we start reading from the beginning: */
	rewind (csv);
	
	/* Fill the tableau array with data read from file: */
	for (row = 0; row <= conc; row++)
	{
		for (col = 0; col < rowsz; col++)
		{
			/* Reset the text buffers prior to reading the cell: */
			cell[0] = ' ';
			cell[1] = '\0';
			
			/* Read the cell character-by-character until a delimiter or newline is reached: */
			rc = (char) fgetc (csv);
			while ((rc != DELIM) && (rc != NEWLN) && (rc != EOF))
			{
				/* Deal with the fact that Windows puts carriage returns on its newlines: */
				if (rc == '\r')
				{
					rc = (char) fgetc (csv);
					continue;
				}
				
				rstr[0] = rc;             /* Insert the latest character into a string */
				strcat (cell, rstr);      /* Concatenate the latest character onto the cell string */
				rc = (char) fgetc (csv);  /* Read the next character from file */
			}
			
			/* Convert the cell text to a number and place it in the tableau: */
			value = strtod (cell, NULL);
			tableau[row][col].c = value;
			tableau[row][col].m = 0;      /* Ensure that the M coefficient gets initialized to 0 */
		}
	}
}

/* count_artificial:
 * * Counts the number of constraints that require an artificial variable
 * 
 * Inputs:
 * * tableau (bigm**):   Pointer to a tableau with no artificial variable columns
 * * varc    (unsigned): Number of decision variables
 * * conc    (unsigned): Number of constraints
 * 
 * Outputs: None
 * 
 * Return Value: (unsigned) Number of artificial variables to create
 */
unsigned count_artificial (bigm **tableau, unsigned varc, unsigned conc)
{
	/* Internal signals: */
	unsigned artc;  /* Number of artificial variables needed */
	unsigned row;   /* Row iterator */
	unsigned zero;  /* 1 if no non-zero values have been encountered; 0 otherwise */
	unsigned col;   /* Column iterator */
	
	for (artc = 0, row = 1; row <= conc; row++)
	{
		for (zero = 1, col = (varc + 1); col <= (varc + conc); col++)
		{
			/* If this constraint has a slack variable, no action is needed: */
			if (tableau[row][col].c == 1)
			{
				zero = 0;
				break;
			}
			
			/* If this constraint has an excess variable, it does need an artificial variable: */
			if (tableau[row][col].c == -1)
			{
				zero = 0;
				artc++;
				break;
			}
		}
		
		/* If the row is an equality constraint, it does need an artificial variable: */
		if (zero)
		{
			artc++;
		}
	}
	
	/* Return the necessary quantity of artificial variables: */
	return artc;
}

/* add_artificial:
 * * Expands a tableau to include artificial variable columns
 * 
 * Inputs:
 * * oldtab (bigm**):   Pointer to a tableau with no artificial variable columns
 * * varc   (unsigned): Number of decision variables
 * * conc   (unsigned): Number of constraints
 * * artc   (unsigned): Number of artificial variables
 * * sense  (int):      -1 for maximization, 1 for minimization
 * 
 * Outputs:
 * * newtab (bigm**):   Pointer to a pre-allocated 2D array for expanded tableau storage
 * 
 * Return Value: None
 */
void add_artificial (bigm **oldtab, bigm **newtab, unsigned varc, unsigned conc, unsigned artc, int sense)
{
	/* Internal storage: */
	unsigned *needed;   /* List of row numbers that require an artificial variable */
	unsigned avar;      /* Artificial variable iterator */
	unsigned row;       /* Row iterator */
	unsigned zero;      /* 1 if no non-zero values have been encountered; 0 otherwise */
	unsigned col;       /* Column iterator */
	bigm     tmp;       /* Temporary storage for M expression being added to others */
	
	/* Create an array of row numbers that need artificial variables: */
	needed = malloc (artc * sizeof(unsigned));
	
	/* Identify the constraints that require an artificial variable: */
	for (avar = 0, row = 1; avar < artc; row++)
	{
		for (zero = 1, col = (varc + 1); col <= (varc + conc); col++)
		{
			/* If the cell contains a 1, that indicates a slack variable; no action is needed for this row: */
			if (oldtab[row][col].c == 1)
			{
				zero = 0;
				break;
			}
			
			/* If the cell contains a -1, that indicates an excess variable; we do need to add an artificial variable: */
			if (oldtab[row][col].c == -1)
			{
				zero = 0;
				needed[avar] = row;
				avar++;
				break;
			}
		}
		
		/* If this row had no coefficient, that indicates an equality constraint; it does need an artificial variable: */
		if (zero)
		{
			needed[avar] = row;
			avar++;
		}
	}
	
	/* Copy existing values into the new tableau: */
	for (row = 0; row <= conc; row++)
	{
		/* Copy the objective, decision, and slack/excess columns: */
		memcpy (newtab[row], oldtab[row], ((1 + varc + conc) * sizeof(bigm)));
		/* Copy the RHS column: */
		newtab[row][1 + varc + conc + artc] = oldtab[row][1 + varc + conc];
	}
	
	/* Populate the artificial variable columns: */
	
	/* Every artificial cell in the net evaluation row gets nonzero M: */
	for (col = (1 + varc + conc); col < (1 + varc + conc + artc); col++)
	{
		newtab[0][col] = multm (_1m0, (sense * -1));
	}
	
	/* Iterate over each cell in the artificial columns to populate it with a value: */
	for (col = (1 + varc + conc); col < (1 + varc + conc + artc); col++)
	{
		for (row = 1; row <= conc; row++)
		{
			/* If the cell's row matches the number needed for this column, give it a coefficient of 1: */
			if (needed[col - (1 + varc + conc)] == row)
			{
				newtab[row][col] = _0m1;
			}
			/* Otherwise, set it to 0: */
			else
			{
				newtab[row][col] = _0m0;
			}
		}
	}
	
	/* Add a Big M term to each of the net evaluation row cells for each artificial variable: */
	for (avar = 0; avar < artc; avar++)
	{
		row = needed[avar];  /* Use values from non-less-than constraint rows as coefficients to the M terms */
		
		for (col = 0; col <= (1 + varc + conc + artc); col++)
		{
			tmp = multm (multm(_1m0, sense), newtab[row][col].c);
			newtab[0][col] = addm (newtab[0][col], tmp);
		}
	}
	
	/* Release memory used to store the needed row numbers: */
	free (needed);
}
