/* Copyright (c) 2019 Kyle Kern <kylekern0@protonmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. */

/* matrix.h:
 * * Elementary row operations function declarations */

#ifndef __MATRIX_H__
#define __MATRIX_H__

/* Project includes: */
#include "bigm.h"

/* Interchange two rows in the matrix: */
void ero_interchange (bigm **matrix, unsigned rowsz, unsigned row1, unsigned row2);

/* Multiply each row element by a nonzero constant: */
void ero_product (bigm **matrix, unsigned rowsz, unsigned dst, double factor);

/* Multiply a row by a constant, then add to another row: */
void ero_sumproduct (bigm **matrix, unsigned rowsz, unsigned dst, unsigned src, double factor);

/* Sum-product logic for Big M factors: */
void ero_sumproduct_m (bigm **matrix, unsigned rowsz, unsigned dst, unsigned src, bigm factor);

#endif // __MATRIX_H__

