/* Copyright (c) 2019 Kyle Kern <kylekern0@protonmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. */

/* simplex.c:
 * * Simplex Algorithm function implementations */

/* Project includes: */
#include "simplex.h"
#include "matrix.h"
#include "globals.h"

/* Standard Library includes: */
#include <stdio.h>

/* check_basic:
 * * Checks if the variable in a given column is a basic variable
 * 
 * Inputs:
 * * tableau (bigm**):   Table of objective and constraint functions
 * *          1st index: Row number
 * *          2nd index: Column number
 * * rowc    (unsigned): Number of rows
 * * varn    (unsigned): Variable (column) number to check
 * 
 * Outputs: None
 * 
 * Return Value: (int) 0 if not basic, 1 otherwise
 */
int check_basic (bigm **tableau, unsigned rowc, unsigned varn)
{
	/* Internal storage: */
	unsigned row;   /* Row iterator */
	unsigned ones;  /* Count of 1 coefficients found */
	
	/* A variable is basic iff only one coefficient in its column is 1; all others must be 0
	 * Iterate over all non-objective-function rows to test this: */
	for (row = 1, ones = 0; row < rowc; row++)
	{
		if ( eqm (tableau[row][varn], _0m0) )
		{
			continue;
		}
		else if ( eqm (tableau[row][varn], _0m1) )
		{
			ones++;
		}
		else
		{
			return 0;
		}
	}
	
	/* If the count of 1s isn't exactly 1, this isn't a basic variable: */
	return ((ones == 1) ? 1 : 0);
}

/* pivot_col:
 * * Determines the column number for the next simplex algo pivot column
 * 
 * Inputs:
 * * tableau (bigm**):   Table of objective and constraint functions
 * *          1st index: Row number
 * *          2nd index: Column number
 * * varc    (unsigned): Number of decision variables
 * * conc    (unsigned): Number of constraints
 * * sense   (int):      -1 for maximization, 1 for minimization
 * 
 * Outputs: None
 * 
 * Return Value: (unsigned) 0 indicates error, otherwise pivot column number
 */
unsigned pivot_col (bigm **tableau, unsigned varc, unsigned conc, int sense)
{
	/* Internal storage: */
	unsigned col;   /* Column iterator */
	bigm     xval;  /* Most extreme value encountered */
	unsigned pcol;  /* Pivot column */
	
	/* Identify the next pivot column as the one with the most extreme row 0 coefficient: */
	for (xval = _0m0, pcol = 0, col = 1; col <= (varc + conc); col++)
	{
		if ( gtm (multm (tableau[0][col], sense), xval) )
		{
			/* Disqualify this column if its variable is basic: */
			if (check_basic (tableau, (1 + conc), col) != 0)
			{
				continue;
			}
			
			/* Otherwise, this becomes the new candidate for pivot column: */
			xval = multm (tableau[0][col], sense);
			pcol = col;
		}
	}
	
	/* Return the identified column (0 indicates error) */
	return pcol;
}

/* pivot_row:
 * * Determines the row number for the next simplex algo pivot row
 * 
 * Inputs:
 * * tableau (bigm**):   Table of objective and constraint functions
 * *          1st index: Row number
 * *          2nd index: Column number
 * * varc    (unsigned): Number of decision variables
 * * conc    (unsigned): Number of constraints
 * * artc    (unsigned): Number of artificial variables
 * * pcol    (unsigned): Pivot row number
 * 
 * Outputs: None
 * 
 * Return Value: (unsigned) 0 indicates error, otherwise pivot row number
 */
unsigned pivot_row (bigm **tableau, unsigned varc, unsigned conc, unsigned artc, unsigned pcol)
{
	/* Internal storage: */
	unsigned row;    /* Row iterator */
	double   xval;   /* Most extreme value encountered */
	unsigned prow;   /* Pivot row */
	double   ratio;  /* Ratio of RHS to pivot column coefficient */
	
	/* Identify the next pivot row as the one with the lowest ratio of RHS to coefficient: */
	for (xval = DBL_MAX, prow = 0, row = 1; row <= conc; row++)
	{
		/* Skip this row if the coefficient is less than or equal to 0: */
		if ( tableau[row][pcol].c <= 0 )
		{
			continue;
		}
		
		/* Otherwise, check to see if this row has the lowest ratio yet: */
		ratio = (tableau[row][varc + conc + artc + 1].c / tableau[row][pcol].c);
		if (ratio < xval)
		{
			/* If this ratio is lower, then this row becomes the next pivot row candidate */
			xval = ratio;
			prow = row;
		}
	}
	
	/* Return the identified row (0 indicates error) */
	return prow;
}

/* simplex:
 * * Solves a linear programming problem via the simplex algorithm
 * 
 * Inputs:
 * * tableau (bigm**):   Table of objective and constraint functions
 * *          1st index: Row number
 * *          2nd index: Column number
 * * varc    (unsigned): Number of decision variables
 * * conc    (unsigned): Number of constraints
 * * artc    (unsigned): Number of artificial variables
 * * sense   (int):      -1 for maximization, 1 for minimization
 * 
 * Outputs:
 * * tableau (bigm**): RHS column contains decision, slack variable values
 * 
 * Return Value: None
 */
void simplex (bigm **tableau, unsigned varc, unsigned conc, unsigned artc, int sense)
{
	/* Internal storage: */
	unsigned col;      /* Column iterator */
	unsigned row;      /* Row iterator */
	unsigned pcol;     /* Pivot column */
	unsigned prow;     /* Pivot row */
	double   dfactor;  /* Factor for constraint row operations */
	bigm     mfactor;  /* Factor for net evaluation row operations */
	
	/* Find the initial pivot column: */
	pcol = pivot_col (tableau, varc, conc, sense);
	
	/* Loop until optimal (no pivot column found): */
	while (pcol != 0)
	{
		/* Identify the next pivot row: */
		prow = pivot_row (tableau, varc, conc, artc, pcol);
		
		/* Convert the pivot row into a unit row with the 1 in the pivot column: */
		dfactor = 1 / tableau[prow][pcol].c;
		ero_product (tableau, (1 + varc + conc + artc + 1), prow, dfactor);
		if (PRINT_OPERATIONS)
		{
			printf ("R%3u *= %+8.3lf\n", prow, dfactor);
		}
		
		/* Manipulate all other rows to have a 0 in the pivot column: */
		for (row = 0; row <= conc; row++)
		{
			/* Skip the current row if:
			 * A. It is the pivot row
			 * B. It already contains a 0 in the pivot column */
			if ((row == prow) || eqm(tableau[row][pcol], _0m0))
			{
				continue;
			}
			
			mfactor = multm (tableau[row][pcol], -1.0);
			ero_sumproduct_m (tableau, (1 + varc + conc + artc + 1), row, prow, mfactor);
			if (PRINT_OPERATIONS)
			{
				if (PRINT_MTERMS && (row == 0))
				{
					printf ("R%3u += (%3.0lfM%+8.3lf) * R%3u\n", row, mfactor.m, mfactor.c, prow);
				}
				else
				{
					printf ("R%3u += %+8.3lf * R%3u\n", row, mfactor.c, prow);
				}
			}
		}
		
		/* Identify the next pivot column: */
		pcol = pivot_col (tableau, varc, conc, sense);
		
		/* Print the intermediate tableaus, if enabled: */
		if (PRINT_INTERMEDIATE)
		{
			/* Prevent double-printing of the final tableau: */
			if (pcol != 0)
			{
				printf ("\n");
				for (row = 0; row <= conc; row++)
				{
					printf ("R%3u: ", row);
					for (col = 0; col < (1+varc+conc+artc+1); col++)
					{
						if (PRINT_MTERMS) printf ("%3.0lfM%+8.3lf ", tableau[row][col].m, tableau[row][col].c);
						else printf ("%+8.3lf ", tableau[row][col].c);
					}
					printf ("\n");
				}
			}
		}
	}
}

/* parse_tableau:
 * * Extracts variable values from final tableau
 * 
 * Inputs:
 * * tableau (bigm**):   Pointer to the final tableau
 * * varc    (unsigned): Number of decision variables
 * * conc    (unsigned): Number of constraints
 * * artc    (unsigned): Number of artificial variables
 * 
 * Outputs:
 * * values  (double*):  Pointer to a pre-allocated 2D array for value storage
 * 
 * Return Value: None
 */
void parse_tableau (bigm **tableau, unsigned varc, unsigned conc, unsigned artc, double *values)
{
	/* Internal storage: */
	unsigned rowsz;  /* Number of cells in one row */
	unsigned row;    /* Row iterator */
	unsigned col;    /* Column iterator */
	
	/* Calculate the size of each row - columns for z, all x, all slack, all artificial, and RHS: */
	rowsz = 1 + varc + conc + artc + 1;
	
	/* Initialize the value array to all 0 - if an RHS value cannot be assigned, the value is 0: */
	for (row = 0; row < (rowsz - 1); row++)
	{
		values[row] = 0;
	}
	
	/* Iterate over each row and assign its RHS to one of the variables: */
	for (row = 0; row <= conc; row++)
	{
		/* Find a unit column with a 1 in this row: */
		for (col = 0; col < (rowsz - 1); col++)
		{
			if ( eqm (tableau[row][col], _0m1) )
			{
				if ( check_basic (tableau, (1 + conc), col) || ((col == 0) && (row == 0)) )  /* || manual override for detecting objective variable column */
				{
					/* If this column satisfies the condition, then its variable takes the row's RHS value: */
					values[col] = tableau[row][rowsz - 1].c;
					break;
				}
			}
		}
	}
}
