/* Copyright (c) 2019 Kyle Kern <kylekern0@protonmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. */

/* tableau.h:
 * * Simplex tableau generation and validation function declarations */

#ifndef __TABLEAU_H__
#define __TABLEAU_H__

/* Project includes: */
#include "bigm.h"

/* CSV reader constants: */
#define DELIM ','           /* Delimiter character */
#define NEWLN '\n'          /* Newline character */
#define MAX_CELL_CHARS 256  /* Maximum number of characters allowed in a cell */

/* Standard Library includes: */
#include <stdio.h>

/* Counts the number of constraint rows in the file: */
unsigned count_constraints (FILE *csv);

/* Counts the number of decision variables in the file: */
unsigned count_variables (FILE *csv, unsigned conc);

/* Imports a simplex tableau from comma-deliminated file: */
void csv_to_tab (bigm **tableau, FILE *csv, unsigned varc, unsigned conc);

/* Counts the number of constraints that require an artificial variable: */
unsigned count_artificial (bigm **tableau, unsigned varc, unsigned conc);

/* Expands a tableau to include artificial variable columns: */
void add_artificial (bigm **oldtab, bigm **newtab, unsigned varc, unsigned conc, unsigned artc, int sense);

#endif // __TABLEAU_H__
