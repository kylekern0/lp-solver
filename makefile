SRC = $(wildcard *.c)
OBJ = $(patsubst %.c,%.o,$(SRC))

all: lpsolver clean

lpsolver: $(OBJ)
	$(CC) $^ -o $@

%.o: %.c
	$(CC) -c $< -o $@

clean:
	rm *.o
