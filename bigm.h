/* Copyright (c) 2019 Kyle Kern <kylekern0@protonmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. */

/* bigm.h:
 * * Big M expression arithmetic and comparison declarations */

#ifndef __BIGM_H__
#define __BIGM_H__

/* Standard Library includes: */
#include <float.h>

/* Structure for storing the Big M-style coefficients in the tableau: */
typedef struct _bigm_struct_
{
	double m;  /* Coefficient to the big M term */
	double c;  /* Constant term */
}
bigm;

/* Big M notation constants: */
extern const bigm _0m0;  /* 0m + 0 */
extern const bigm _0m1;  /* 0m + 1 */
extern const bigm _1m0;  /* 1m + 0 */
extern const bigm MAXM;  /* Largest possible value */

/* Multiplies a Big M expression a by a constant b: */
bigm multm (bigm a, double b);

/* Add two Big M expressions: */
bigm addm (bigm a, bigm b);

/* Less-than comparison for Big M expressions: */
int ltm (bigm a, bigm b);

/* Greater-than comparison for Big M expressions: */
int gtm (bigm a, bigm b);

/* Equal-to comparison for Big M expressions: */
int eqm (bigm a, bigm b);

/* Less-than-or-equal comparison for Big M expressions: */
int ltem (bigm a, bigm b);

/* Greater-than-or-equal comparison for Big M expressions: */
int gtem (bigm a, bigm b);

#endif // __BIGM_H__
