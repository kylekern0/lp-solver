/* Copyright (c) 2019 Kyle Kern <kylekern0@protonmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. */

/* csvtest.c:
 * * Simplex algo LP solver using tableau imported from CSV file */

/* Project includes: */
#include "simplex.h"
#include "tableau.h"
#include "bigm.h"
#include "globals.h"

/* Standard Library includes: */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* Clear global option flags by default: */
int PRINT_MTERMS = 0;
int PRINT_ARTIFICIAL = 0;
int PRINT_FINAL = 0;
int PRINT_INTERMEDIATE = 0;
int PRINT_OPERATIONS = 0;

/* Program entry point: */
int main (int argc, char **argv)
{
	/* Internal storage: */
	int      sense;      /* -1 for maximization, 1 for minimization */
	int      i;          /* Option iterator */
	int      j;          /* Option character iterator */
	unsigned conc;       /* Number of constraints */
	unsigned varc;       /* Number of variables */
	unsigned rowsz;      /* Number of columns in one row */
	bigm     **inittab;  /* Simplex tableau */
	char     *csvname;   /* Name of the CSV file to import */
	FILE     *csv;       /* File pointer to CSV tableau file */
	unsigned row;        /* Row iterator */
	unsigned col;        /* Column iterator */
	unsigned artc;       /* Number of artificial variables */
	bigm     **arttab;   /* Simplex tableau with necessary artificial variables */
	double   *varvals;   /* Variable values after optimization */
	double   *shadows;   /* Shadow prices */
	
	/* Attribution: */
	printf ("lpsolver 20191224 by Kyle Kern <kylekern0@protonmail.com>.\nThis is free software; you may use and redistribute it freely.\nThere is NO WARRANTY, to the extent permitted by law.\n");
	
	/* Interpret command-line arguments: */
	/* If there are too few arguments, print a help message: */
	if (argc < 3)
	{
		printf ("\nSolves linear programming optimization problems using the big M method\n");
		printf ("\n");
		printf ("Usage:\n");
		printf ("\t%s [SENSE] [CSVFILE] {OPTIONS}\n", argv[0]);
		printf ("Where:\n");
		printf ("\t[SENSE]   = 'max' for maximization, or 'min' for minimization\n");
		printf ("\t[CSVFILE] = name of comma-delimited file containing the initial tableau\n");
		printf ("Options:\n");
		printf ("\t-m = print M-coefficients\n");
		printf ("\t-a = print initial tableau with artificial variables\n");
		printf ("\t-f = print final tableau\n");
		printf ("\t-t = print all tableaus (implies -a and -f)\n");
		printf ("\t-r = print row operations between tableaus\n");
		printf ("\n");
		printf ("The initial tableau file shall be given in tableau standard form, delimiting\n");
		printf ("variables with ',' and rows with newlines. Do not include leading or trailing\n");
		printf ("whitespace. Include a column for each row's slack/excess variable, even if the\n");
		printf ("constraint is equality. Artificial variable columns are added automatically.\n");
		
		return -1;  /* Too few arguments exit condition */
	}
	
	/* Otherwise, determine problem sense from command line: */
	if (! (strcmp (argv[1], "max") && strcmp (argv[1], "maximize")))
	{
		sense = -1;  /* Use negative sense factor for max problems */
	}
	else if (! (strcmp (argv[1], "min") && strcmp (argv[1], "minimize")))
	{
		sense = 1;   /* Use positive sense factor for min problems */
	}
	else  /* Otherwise, exit with error: */
	{
		printf ("Error: '%s': invalid problem sense\n", argv[1]);
		return -2;  /* Invalid sense exit condition */
	}
	
	/* Interpret command-line options: */
	for (i = 3; i < argc; i++)
	{
		/* Verify that the option starts with the - character: */
		if (argv[i][0] != '-')
		{
			printf ("Warning: Ignoring '%s': options must begin with -\n", argv[i]);
			continue;
		}
		
		/* Set flags according to the valid options provided: */
		for (j = 1; argv[i][j] != '\0'; j++)
		{
			switch (argv[i][j])
			{
				case 'm': PRINT_MTERMS = 1; break;        /* -m enables M-term printing */
				case 'a': PRINT_ARTIFICIAL = 1; break;    /* -a enables artificial table printing */
				case 'f': PRINT_FINAL = 1; break;         /* -f enables final table printing */
				case 't': PRINT_INTERMEDIATE = 1; break;  /* -t enables printing all tables */
				case 'r': PRINT_OPERATIONS = 1; break;    /* -r enables printing row operations */
				
				/* If the option is none of the above, then print a warning: */
				default:
				{
					printf ("Warning: Ignoring '%c': not an option\n", argv[i][j]);
					break;
				}
			}
		}
	}
	
	/* Open the CSV file: */
	csv = fopen (argv[2], "r");
	if (csv == NULL)
	{
		/* Print error and exit upon failed open: */
		printf ("Error: failed to open '%s' for reading\n", argv[2]);
		return -3;  /* Invalid input file name exit condition */
	}
	
	/* Count constraints and variables: */
	conc = count_constraints (csv);
	varc = count_variables (csv, conc);
	
	/* Allocate space for the tableau: */
	rowsz = (1 + varc + conc + 1);
	inittab = malloc ((1 + conc) * sizeof(bigm*));
	for (row = 0; row <= conc; row++)
	{
		inittab[row] = malloc (rowsz * sizeof(bigm));
	}
	
	/* Import the data from the CSV: */
	csv_to_tab (inittab, csv, varc, conc);
	
	/* Print the initial tableau: */
	printf ("\nProblem: %s\n=================\n", ((sense == -1) ? "Maximize" : "Minimize"));
	for (row = 0; row <= conc; row++)
	{
		printf ("R%3u: ", row);
		for (col = 0; col < rowsz; col++)
		{
			printf ("%+8.3lf ", inittab[row][col].c);
		}
		printf ("\n");
	}
	
	/* Count necessary artificial variables: */
	artc = count_artificial (inittab, varc, conc);
	
	/* Allocate a new tableau large enough to hold the artificial variables too: */
	arttab = malloc ((1 + conc) * sizeof(bigm*));
	for (row = 0; row <= conc; row++)
	{
		arttab[row] = malloc ((rowsz + artc) * sizeof(bigm));
	}
	
	/* Generate artificial variable columns into the new tableau: */
	add_artificial (inittab, arttab, varc, conc, artc, sense);
	
	/* Print the modified tableau iff artificial variables were added: */
	if ((PRINT_ARTIFICIAL || PRINT_INTERMEDIATE) && (artc > 0))
	{
		printf ("\nMODIFIED TABLEAU\n");
		for (row = 0; row <= conc; row++)
		{
			printf ("R%3u: ", row);
			for (col = 0; col < (rowsz + artc); col++)
			{
				if (PRINT_MTERMS) printf ("%3.0lfM%+8.3lf ", arttab[row][col].m, arttab[row][col].c);
				else printf ("%+8.3lf ", arttab[row][col].c);
			}
			printf ("\n");
		}
	}
	
	/* Run the simplex algorithm: */
	simplex (arttab, varc, conc, artc, sense);
	
	/* Print the final tableau: */
	if (PRINT_FINAL || PRINT_INTERMEDIATE)
	{
		printf ("\nFINAL TABLEAU\n");
		for (row = 0; row <= conc; row++)
		{
			printf ("R%3u: ", row);
			for (col = 0; col < (rowsz + artc); col++)
			{
				if (PRINT_MTERMS) printf ("%3.0lfM%+8.3lf ", arttab[row][col].m, arttab[row][col].c);
				else printf ("%+8.3lf ", arttab[row][col].c);
			}
			printf ("\n");
		}
	}
	
	/* Isolate the variables' values: */
	varvals = malloc ((rowsz + artc - 1) * sizeof(double));
	parse_tableau (arttab, varc, conc, artc, varvals);
	
	/* Display the variable values: */
	printf ("\n\nlpsolver analysis\n=================\n");
	printf ("%s objective: %+8.3lf\n", ((sense == -1) ? "Maximized" : "Minimized"), varvals[0]);
	printf ("\nDecision variables:      \tReduced Costs:\n");
	for (col = 1; col <= varc; col++)
	{
		printf ("\tVar%3u = %+8.3lf\t\t", col, varvals[col]);
		
		/* Reduced costs are derived from the decision variables' contributions to the objective: */
		printf ("%+8.3lf\n", (-1 * arttab[0][col].c));
	}
	printf ("\nConstraints slack/excess:\tShadow Prices:\n");
	for (; (col - varc) <= conc; col ++)
	{
		printf ("\tRow%3u = %+8.3lf\t\t", (col - varc), varvals[col]);
		
		/* Shadow prices are derived from the slack/excess variables' contributions to the objective: */
		printf ("%+8.3lf\n", arttab[0][col].c);
	}
	
	return 0;
}
